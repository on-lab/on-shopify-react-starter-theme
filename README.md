## ON Lab's React Starter Theme for Shopify

Based on Shopify's starter template, initially created by [@liron-navon](https://github.com/liron-navon), and tweaked by [ON Lab](http://on-lab.com).

Thanks to [@dan-gamble](https://github.com/dan-gamble) for helping with the slate configurations

This project is based off [shopify/skeleton-theme](https://github.com/shopify/skeleton-theme), checkout [Slate](https://github.com/Shopify/slate) for more information.

### Get started

Install the theme with npx:
```
yarn create slate-theme <theme-name> https://bitbucket.org/on-lab/on-shopify-react-starter-theme
```

Replace `theme-name` with the name you wish to use for your theme.


### Getting started

#### Option 1: Getting started manually

- rename .env-sample to .env and setup your development store config.
- run: yarn start 
- slate will open your localhost, just ignore that, and go to your development store. 
- In the development store you should have your theme loaded
- Changes will hot reload in the dev store.

#### Options 2: Getting started using Shopify SBM

`npm shopify-sbm init -d <domain> -k <key> -p <password>`

- domain is you shopify domain (*.myshopify.com), 
- key and password are from a private app.

Learn more about how to set up this private app [here](https://github.com/nicolasalliaume/on-shopify-cli#create-a-private-app).


#### How to mount components


```js
import ThemeComponent from '../../react/layout/theme.jsx';
import { mountReact } from '../../react/mountReact';

// the component is rendered to replace the selected html element
mountReact( ThemeComponent, '#root' );
```


#### Changes from the Shopify barebone Theme

Some changes where originally made by the creator ([@liron-navon](https://github.com/liron-navon)). Taken from his GitHub README.md:

Do notice I tried to change as little as possible to allow this to be updated when shopify will change the starter template,
The only files changed are:

1. scripts/layout/theme.js : added a function to load the react component.
2. layout/theme.liquid : added an anchor for react to load onto.
3. created /react directory which contain all of the react logic.

##### Additional changes made by ON Lab

1. Added lint function and lint config files from [starter-theme](https://github.com/Shopify/starter-theme).
2. Created an initial react/ directory structure for layouts and components.
3. Created a Theme component to serve as root component of the theme.
4. Created a ThemeLayout component to render a default theme layout.


## Cheatsheet

Find a Cheatsheet with ON Lab's Shopify Development process using this and other tools [here](https://onlab-tmp-bucket.s3-us-west-2.amazonaws.com/ON+Lab+-+Shopify+Development+Cheatsheet+.pdf).


## Collaboration

If you wish to add a new base functionality for this theme, create a Pull Request and one of the authors/maintainers of this project will review it.


--------

[![ON Lab](http://on-lab.com/on-lab.jpg)](http://on-lab.com)
