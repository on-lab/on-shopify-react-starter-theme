/**
 * Shopify Cart ajax operations.
 *
 * 	- Add an variant
 * 	- Update a line
 * 	- Remove a line
 * 	- Fetch cart
 *
 * All operations return a promise that resolves
 * to the updated cart.
 *
 */
class CartAPI {
	addItem = ( id, quantity = 1, properties = {} ) => {
		return fetch( '/cart/add.js', {
			method: 'POST',
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify( { id, quantity, properties } ),
		} )
			.then( ( r ) => r.json() )
			.then( ( json ) => {
				if ( this.isCartError( json ) ) {
					throw Error( json.description );
				}
				return json;
			} )
			.then( () => this.getCart() );
	}

	removeLine = ( line ) => {
		return this.updateLine( line, 0 );
	}

	updateLine = ( line, quantity ) => {
		return fetch( '/cart/change.js', {
			method: 'POST',
			credentials: 'same-origin',
			body: JSON.stringify( { line, quantity } ),
			headers: {
				'Content-Type': 'application/json',
			},
		} )
			.then( ( r ) => r.json() )
			.then( ( cart ) => {
				if ( this.isCartError( cart ) ) {
					throw Error( cart.description );
				}
				return cart;
			} );
	}

	getCart = () => {
		return fetch( '/cart.js', {
			credentials: 'same-origin',
		} )
			.then( ( r ) => r.json() )
			.then( ( cart ) => {
				if ( this.isCartError( cart ) ) {
					throw Error( cart.description );
				}
				return cart;
			} );
	}

	isCartError = ( json ) => {
		return json.status === 422 || json.status === 'bad_request';
	}
}

export default new CartAPI();
