class Section {
	constructor( { id, name, settings, blocks } ) {
		this.id = id;
		this.name = name.replace( /\s/g, '-' ).toLowerCase();
		this.settings = settings;
		this.blocks = blocks.map( ( b ) => new Block( b ) );
		this.isDynamic = id !== name;
	}

	getSetting = ( id ) => this.settings[ id ]
}

class Block {
	constructor( { type, settings } ) {
		this.type = type;
		this.settings = settings;
	}

	getSetting = ( id ) => this.settings[ id ]
}

/**
 * Use * to get all sections.
 * Use section id to get just one.
 *
 * @param  {string} sectionId Section ID or '*'
 * @return {Section} The section(s)
 */
export default ( sectionId = '*' ) => {
	if ( sectionId === '*' ) {
		return window.theme.sections.map( ( s ) => new Section( s ) );
	}
	const section = window.theme.sections.find( ( { id } ) => id === sectionId );
	if ( ! section ) {
		throw new Error( `Section ${ sectionId } not found in theme.sections` );
	}
	return new Section( section );
};
