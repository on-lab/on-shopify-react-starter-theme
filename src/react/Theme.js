import 'isomorphic-fetch';
import React from 'react';
import { Provider } from 'react-redux';
import ThemeLayout from './layout/ThemeLayout';
import HeaderSection from './components/header/HeaderSection';
import Main from './components/Main';
import Footer from './components/footer/Footer';
import Router from './Router';
import store from './store';

const Theme = () => {
	return (
		<Provider store={ store }>
			<ThemeLayout>
				<HeaderSection />
				<Main><Router /></Main>
				<Footer />
			</ThemeLayout>
		</Provider>
	);
};

export default Theme;
