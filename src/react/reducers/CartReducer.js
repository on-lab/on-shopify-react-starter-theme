import useCart from '../hooks/useCart';

const intitialCart = useCart();

export default ( state = intitialCart, action ) => {
	switch ( action.type ) {
		case 'UPDATE_CART': {
			return action.payload;
		}
		default: return state;
	}
};
