import React from 'react';

const EmptyCartMessage = () => {
	return (
		<React.Fragment>
			<p>Your cart is currently empty.</p>
			<p><a href="/collections/all">Browse products</a></p>
		</React.Fragment>
	);
};

export default EmptyCartMessage;
