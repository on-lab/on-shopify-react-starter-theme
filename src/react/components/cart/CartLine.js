import React from 'react';
import QuantitySelector from '../common/QuantitySelector';

const CartLines = ( { item, number, handleUpdate } ) => {
	return (
		<div className='cart__line' data-line={ number }>
			<p>{ item.product_title }</p>

			<QuantitySelector
				quantity={ item.quantity }
				handleUpdate={ handleUpdate }
			/>
		</div>
	);
};

export default CartLines;
