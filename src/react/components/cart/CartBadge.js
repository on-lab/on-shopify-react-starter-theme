import React from 'react';
import { useSelector } from 'react-redux';

const CartBadge = () => {
	const cart = useSelector( ( state ) => state.cart );
	const count = cart.item_count;

	return <span>{ count }</span>;
};

export default CartBadge;
