import React from 'react';
import EmptyCartMessage from './EmptyCartMessage';
import CartLine from './CartLine';

const CartLines = ( props ) => {
	const {
		cart,
		handleLineUpdate,
	} = props;

	const isEmpty = cart.item_count === 0;
	if ( isEmpty ) {
		return <EmptyCartMessage />;
	}

	return (
		<div className='cart__lines'>
			{ cart.items.map( ( line, index ) =>
				<CartLine
					key={ line.id }
					item={ line }
					number={ index + 1 }
					handleUpdate={ ( quantity ) => handleLineUpdate( index + 1, quantity ) }
				/>
			) }
		</div>
	);
};

export default CartLines;
