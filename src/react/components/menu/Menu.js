import React from 'react';
import classnames from 'classnames';

const Menu = ( { menu, className, submenu, ...otherProps } ) => {
	return (
		<ul className={ classnames( { submenu, menu: ! submenu }, className ) }>
			{ menu.map( ( child ) => <MenuItem item={ child } key={ child.title } /> ) }
		</ul>
	);
};

const MenuItem = ( { item: { url, title, children } } ) => (
	<li className='menu__item'>
		<a href={ url }>
			<span>{ title }</span>
		</a>
		{ children && <Menu submenu menu={ children } /> }
	</li>
);

export default Menu;
