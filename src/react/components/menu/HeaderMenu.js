import React from 'react';
import useSections from '../../hooks/useSections';
import Menu from './Menu';

const HeaderMenu = ( props ) => {
	const menu = useSections( 'header' ).settings.menu;
	return (
		<div id="header-menu">
			<Menu menu={ menu } className='menu--header' />
		</div>
	);
};

export default HeaderMenu;
