import React from 'react';
import Button from '../common/Button';

const AddToCartButton = ( { children, ...otherProps } ) =>
	<Button {...otherProps} primary>{ children }</Button>;

export default AddToCartButton;
