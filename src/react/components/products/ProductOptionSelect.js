import React from 'react';
import Select from '../common/Select';

const ProductOptionSelect = ( { product, option, onChange, ...otherProps } ) => {
	const { name, values } = option;
	const options = values.map( ( value ) => ( {
		value,
		label: value,
	} ) );

	return (
		<Select
			options={ options }
			onChange={ ( event ) => onChange( option, event.target.value ) }
			{ ...otherProps }
		/>
	);
};

export default ProductOptionSelect;
