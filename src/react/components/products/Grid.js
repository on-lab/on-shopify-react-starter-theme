import React from 'react';
import classnames from 'classnames';

const Grid = ( { className, children, ...otherProps } ) => {
	return (
		<div className={classnames( [ 'grid', className ] )} {...otherProps}>
			{ children }
		</div>
	);
};

export default Grid;
