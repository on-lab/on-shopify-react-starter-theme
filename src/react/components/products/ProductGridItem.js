import React from 'react';

const ProductGridItem = ( { product } ) => {
	return (
		<p>
			<a href={ `/products/${ product.handle }` }>
				{ product.title }
			</a>
		</p>
	);
};

export default ProductGridItem;
