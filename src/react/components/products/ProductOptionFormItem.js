import React from 'react';
import classnames from 'classnames';
import ProductOptionSelect from './ProductOptionSelect';

const ProductOptionFormItem = ( {
	option,
	product,
	value,
	onChange,
	className,
} ) => (
	<div className={ classnames( 'product__options-form__option', className ) }>
		<label>{ option.name }</label>
		<ProductOptionSelect
			product={ product }
			option={ option }
			value={ value }
			onChange={ onChange }
		/>
	</div>
);

export default ProductOptionFormItem;
