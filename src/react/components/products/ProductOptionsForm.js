import React from 'react';
import classnames from 'classnames';
import ProductOptionFormItem from './ProductOptionFormItem';

const ProductOptionsForm = ( {
	product,
	variant,
	onChange,
	className,
	...otherProps
} ) => {
	const handleOptionChange = ( option, value ) => {
		const optionIndex = option.position - 1;
		const options = [ ...variant.options ];
		options[ optionIndex ] = value;

		const newVariant = product.variants.find(
			( v ) => JSON.stringify( v.options ) === JSON.stringify( options )
		);
		onChange( newVariant );
	};

	return (
		<form
			{ ...otherProps }
			onSubmit={ () => {} }
			className={ classnames( 'product__options-form', className ) }
		>
			{ product.options_with_values.map( ( option, index ) => (
				<ProductOptionFormItem
					key={ option.name }
					product={ product }
					option={ option }
					value={ variant.options[ index ] }
					onChange={ handleOptionChange }
				/>
			) ) }
		</form>
	);
};

export default ProductOptionsForm;
