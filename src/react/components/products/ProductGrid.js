import React from 'react';
import Grid from './Grid';
import ProductGridItem from './ProductGridItem';
import Pagination from '../common/Pagination';

const ProductGrid = ( { products, pagination } ) => {
	return (
		<Grid>
			{ products.map(
				( product ) => <ProductGridItem
					key={ product.id }
					product={ product }
				/>
			) }
			<Pagination pagination={ pagination } />
		</Grid>
	);
};

export default ProductGrid;
