import React from 'react';

const FeaturedCollectionSection = ( { section } ) => {
	const { collection } = section.settings;
	return (
		<div className='section--featured-collection'>
			{ collection.products.map( ( product ) => (
				<p key={ product.id }>
					<a href={ `/products/${ product.handle }` }>
						{ product.title }
					</a>
				</p>
			) ) }
		</div>
	);
};

export default FeaturedCollectionSection;
