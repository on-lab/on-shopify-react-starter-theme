import React from 'react';
import FeaturedCollectionSection from './FeaturedCollectionSection';

const Section = ( { section } ) => {
	const mapping = {
		'featured-collection': <FeaturedCollectionSection section={ section } />,
	};

	return mapping[ section.name ] || (
		<div>
			{ `Section ${ section.name } not implemented. See DynamicSection.js` }
		</div>
	);
};

export default Section;
