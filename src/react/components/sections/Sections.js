import React from 'react';
import Section from './Section';

const Sections = ( { sections } ) => {
	return (
		<React.Fragment>
			{ sections.map( ( section ) => (
				<Section key={section.id} section={section} />
			) ) }
		</React.Fragment>
	);
};

export default Sections;
