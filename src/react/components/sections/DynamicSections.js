import React from 'react';
import Sections from './Sections';

const DynamicSections = ( { sections } ) =>
	<Sections sections={ sections.filter( ( s ) => s.isDynamic ) } />;

export default DynamicSections;
