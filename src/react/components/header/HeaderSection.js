import React from 'react';
import useSections from '../../hooks/useSections';
import useShop from '../../hooks/useShop';
import Header from './Header';

const HeaderSection = () => {
	const section = useSections( 'header' );
	const shop = useShop();

	const logo = section.getSetting( 'logo' );
	const companyName = shop.name;

	return <Header logo={ logo } companyName={ companyName } />;
};

export default HeaderSection;
