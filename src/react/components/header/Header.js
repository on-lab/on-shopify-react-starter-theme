import React from 'react';
import BrandLogo from './BrandLogo';
import HeaderMenu from '../menu/HeaderMenu';
import CartBadge from '../cart/CartBadge';

const Header = ( { logo, companyName } ) => {
	return (
		<header className='site-header'>
			<div className='site-header__grid'>
				<BrandLogo src={ logo } alt={ companyName } />
				<HeaderMenu />
				<CartBadge />
			</div>
		</header>
	);
};

export default Header;
