import React from 'react';

const BrandLogo = ( { src, alt } ) => {
	return (
		<img
			id="logo-img"
			src={ src }
			alt={ alt }
			title={ alt }
		/>
	);
};

export default BrandLogo;
