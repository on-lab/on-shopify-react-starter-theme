import React from 'react';

const Description = ( { product } ) =>
	<p dangerouslySetInnerHTML={{ __html: product.description }}></p>;

export default Description;
