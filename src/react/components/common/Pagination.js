import React from 'react';
import classnames from 'classnames';

const Pagination = ( { className, pagination } ) => {
	const classes = [ 'pagination', className ];

	const prev = pagination.previous && (
		<a href={ pagination.previous.url }
			dangerouslySetInnerHTML={{ __html: pagination.previous.title }}
		/>
	);

	const next = pagination.next && (
		<a href={ pagination.next.url }
			dangerouslySetInnerHTML={{ __html: pagination.next.title }}
		/>
	);

	const wrapInLink = ( url, title, key ) => (
		<a key={ key }
			href={ url }
			dangerouslySetInnerHTML={{ __html: title }}
		/>
	);

	const wrapInSpan = ( title, key ) => (
		<span key={ key } dangerouslySetInnerHTML={{ __html: title }} />
	);

	const pages = pagination.parts.map( ( part ) => {
		return part.is_link ?
			wrapInLink( part.url, `${ part.title }`, part.title ) :
			wrapInSpan( `${ part.title }`, part.title );
	} );

	return (
		<div className={ classes }>
			{ prev }
			{ pages }
			{ next }
		</div>
	);
};

export default Pagination;
