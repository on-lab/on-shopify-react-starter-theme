import React from 'react';
import classnames from 'classnames';

const Select = ( { options, className, ...otherProps } ) => {
	return (
		<select
			className={ classnames( [ 'select', className ] ) }
			{...otherProps}
		>
			{ options.map( ( option ) => (
				<option
					key={ option.value }
					value={ option.value }
				>
					{ option.label }
				</option>
			) ) }
		</select>
	);
};

export default Select;
