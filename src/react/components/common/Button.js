import React from 'react';
import classnames from 'classnames';

const Button = ( {
	className,
	primary,
	secondary,
	accent,
	link,
	children,
	plain,
	...otherProps }
) => (
	<button
		type='button'
		{ ...otherProps }
		className={classnames( [
			className,
			'button',
			primary ? 'button--primary' :
				secondary ? 'button--secondary' :
					'',
			accent ? 'button--accent' : '',
			plain ? 'button--plain' : '',
			link ? 'button--link' : '',
		] )}
	>
		{ children }
	</button>
);

export default Button;
