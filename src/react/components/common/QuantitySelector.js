import React from 'react';
import classnames from 'classnames';
import Button from './Button';

const QuantitySelector = ( props ) => {
	const {
		className,
		quantity,
		handleUpdate,
	} = props;

	const handleIncrease = () => {
		handleUpdate( quantity + 1 );
	};

	const handleDecrease = () => {
		handleUpdate( quantity - 1 );
	};

	return (
		<div className={ classnames( [ 'quantity-selector', className ] ) }>
			<Button secondary onClick={ handleDecrease }>-</Button>
			<label>{ quantity }</label>
			<Button secondary onClick={ handleIncrease }>+</Button>
		</div>
	);
};

export default QuantitySelector;
