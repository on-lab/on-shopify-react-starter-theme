import React from 'react';
import classnames from 'classnames';

const Image = ( { className, ...otherProps } ) =>
	<img {...otherProps} className={classnames( [ 'image', className ] )} />;

export default Image;
