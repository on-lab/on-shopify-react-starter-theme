import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import cart from './reducers/CartReducer';

const reducers = combineReducers( {
	cart,
} );

const middleware = [
	thunk,
];

export default createStore(
	reducers,
	compose(
		applyMiddleware( ...middleware ),
		window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : ( f ) => f
	)
);
