import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addToCart } from '../actions/cart';
import useView from '../hooks/useView';
import Description from '../components/common/Description';
import Image from '../components/common/Image';
import AddToCartButton from '../components/products/AddToCartButton';
import ProductOptionsForm from '../components/products/ProductOptionsForm';
import { moveToCart } from '../utils/navigation';

const Product = ( props ) => {
	const product = useView().object;
	const dispatch = useDispatch();
	const [ selectedVariant, setSelectedVariant ] = useState( product.variants[ 0 ] );

	const handleAddToCart = () => {
		dispatch( addToCart( selectedVariant.id, 1 ) ).then( moveToCart );
	};

	const handleSelectVariant = ( variant ) => setSelectedVariant( variant );

	return (
		<React.Fragment>
			<h1>{ product.title }</h1>
			<Image src={ product.featured_image } alt={ product.title } />
			<ProductOptionsForm
				product={ product }
				variant={ selectedVariant }
				onChange={ handleSelectVariant }
			/>
			<AddToCartButton
				disabled={ ! selectedVariant.available }
				onClick={ handleAddToCart }
			>
				{ selectedVariant.available && 'Add to cart' }
				{ ! selectedVariant.available && 'Sold out' }
			</AddToCartButton>
			<Description product={ product } />
		</React.Fragment>
	);
};

export default Product;
