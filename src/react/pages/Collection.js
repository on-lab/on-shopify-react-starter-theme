import React from 'react';
import useView from '../hooks/useView';
import ProductGrid from '../components/products/ProductGrid';

const Collection = ( props ) => {
	const { object, pagination } = useView();
	const { products, title } = object;

	return (
		<React.Fragment>
			<h1>{ title }</h1>
			<ProductGrid products={ products } pagination={ pagination } />
		</React.Fragment>
	);
};

export default Collection;
