import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import CartLines from '../components/cart/CartLines';
import { updateLine } from '../actions/cart';

const Cart = ( props ) => {
	const cart = useSelector( ( state ) => state.cart );
	const dispatch = useDispatch();

	const handleLineUpdate = ( line, quantity ) => {
		dispatch( updateLine( line, quantity ) );
	};

	return (
		<CartLines
			cart={ cart }
			handleLineUpdate={ handleLineUpdate }
		/>
	);
};

export default Cart;
