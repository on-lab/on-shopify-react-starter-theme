import React from 'react';
import useSections from '../hooks/useSections';
import DynamicSections from '../components/sections/DynamicSections';

const Index = ( props ) => {
	const sections = useSections( '*' );

	return (
		<DynamicSections sections={ sections } />
	);
};

export default Index;
