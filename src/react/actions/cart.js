import { cart } from '../shopify-api/api';

export const updateCart = ( cart ) => {
	return {
		type: 'UPDATE_CART',
		payload: cart,
	};
};

export const addToCart = ( variantId, quantity ) => {
	return ( dispatch ) => {
		return cart.addItem( variantId, quantity )
			.then( ( cart ) => dispatch( updateCart( cart ) ) );
	};
};

export const updateLine = ( line, quantity ) => {
	return ( dispatch ) => {
		return cart.updateLine( line, quantity )
			.then( ( cart ) => dispatch( updateCart( cart ) ) );
	};
};
