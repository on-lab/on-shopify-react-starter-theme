import React from 'react';
import useView from './hooks/useView';
import Index from './pages/Index';
import Collection from './pages/Collection';
import Product from './pages/Product';
import Login from './pages/Login';
import Account from './pages/Account';
import Cart from './pages/Cart';
import Search from './pages/Search';
import Page from './pages/Page';

const Router = ( props ) => {
	const template = useView().template;

	const routes = {
		index: Index,
		page: Page,
		product: Product,
		collection: Collection,
		cart: Cart,
		search: Search,
		login: Login,
		account: Account,

		/*
			May use template.suffix here to route
			to specific pages (like when creating a
			page.contact or a product.earring template).
		 */
	};

	const page = routes[ template.name ];

	if ( ! page ) {
		return <div>Routing error</div>;
	}

	return React.createElement( page );
};

export default Router;
