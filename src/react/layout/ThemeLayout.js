import React from 'react';
import classnames from 'classnames';
import useView from '../hooks/useView';

const ThemeLayout = ( props ) => {
	const template = useView().template;

	return (
		<div className={classnames( 'layout', `template-${ template.name }` )}>
			{ props.children }
		</div>
	);
};

export default ThemeLayout;
