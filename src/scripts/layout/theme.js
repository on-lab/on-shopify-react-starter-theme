import '../../styles/theme.scss';
import '../../styles/theme.scss.liquid';
import { mountReact } from '../../react/mountReact';
import Theme from '../../react/Theme';

mountReact( '#root', Theme );
